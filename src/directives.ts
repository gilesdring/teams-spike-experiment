import { SchemaDirectiveVisitor } from '@graphql-tools/utils';
import { defaultFieldResolver, GraphQLField } from 'graphql';

function checkRoles(roles: string[], needed: string | string[]) {
  if (Array.isArray(needed)) {
    return needed.map((r) => roles.includes(r)).filter((r) => r).length > 0;
  } else {
    if (roles.includes(needed)) return true;
  }

  return false;
}

export class HasRoleDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: GraphQLField<any, any>) {
    const { resolve = defaultFieldResolver } = field;
    field.resolve = async (...args) => {
      const context = args[2];
      const roles = context.req.headers['x-roles'];
      if (checkRoles(roles, this.args.roles)) return resolve.apply(this, args);
      return undefined;
    };
  }
}
