import { Injectable } from '@nestjs/common';

/*
Create test data

merge (p:Context:Spike:Platform)-[:DEFINES]->(h:Hat:Spike {name: 'GettingSupport'}) return p, h;
merge (t:Context:Spike:Team {name: 'A Team'})-[:DEFINES]->(h:Hat:Spike {name: 'Fool'}) return t, h;
match (h:Hat:Spike)
merge (p:Person:Spike { name: "Hannibal"})
merge (p)-[:WEARS {request_date: date('2021-11-02'), status: 'Pending'}]->(h)
return p, h;



*/

@Injectable()
export class GraphDbService {
  getPerson(id: string) {
    /*
      MATCH (p:Person:Spike)
      WHERE id(p) = $id
      RETURN properties(p) AS person;
     */
    return { name: 'Hannibal' };
  }

  getHatsForPerson() {
    /*
      MATCH (p:Person:Spike)-[w:WEARS]-(h:Hat)-[:DEFINES]-(c:Context)
      WHERE id(p) = 150
      RETURN w {
        .*,
        name: h.name,
        id: id(h)
        context: c { .name, id: id(c) },
      } AS hats;
     */
    return [
      {
        context: { name: null, id: 144 },
        name: 'GettingSupport',
        id: 145,
        request_date: '2021-11-02',
        status: 'Pending',
      },
      {
        context: { name: 'A Team', id: 146 },
        name: 'Fool',
        id: 147,
        request_date: '2021-11-02',
        status: 'Pending',
      },
    ];
  }

  getHatsForContext(contextId: number) {
    return this.getHatsForPerson().filter((x) => x.context.id == contextId);
  }
}
