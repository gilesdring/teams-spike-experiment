import { Resolver, Query, ResolveField } from '@nestjs/graphql';
import { GraphDbService } from '../graph-db/graph-db.service';
import { Person } from './types';

@Resolver(() => Person)
export class PersonResolver {
  constructor(private readonly db: GraphDbService) {}
  @Query(() => Person, {
    name: 'Person',
  })
  person() {
    return this.db.getPerson('1234');
  }

  @ResolveField()
  hats() {
    return this.db.getHatsForPerson();
  }
}
