import { Directive, Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Profile {
  @Field({ nullable: true })
  publicField?: string;

  @Directive('@hasRole(roles: ["platform:admin", "team:member"])')
  @Field({ nullable: true })
  protectedField?: string;

  @Field({ nullable: true })
  nullField?: string;
}
