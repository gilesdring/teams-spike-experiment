import { Resolver, Query } from '@nestjs/graphql';
import { Profile } from './types';

@Resolver(() => Profile)
export class ProfileResolver {
  @Query(() => Profile, {
    name: 'Profile',
  })
  profile(): Profile {
    return {
      publicField: 'I am public',
      protectedField: 'I am protected',
    };
  }
}
